package dathtanim.tanawat.lab7;

import java.awt.geom.Ellipse2D;

public class ball extends Ellipse2D.Double {

	private static final long serialVersionUID = 1L;

	public ball(int x, int y, int BALL_R) {
		super(x, y, BALL_R * 2, BALL_R * 2);
	}
}
