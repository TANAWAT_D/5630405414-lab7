package dathtanim.tanawat.lab7;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class WrapAroundBall extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;

	private final static int WIDTH = 600;
	private final static int HEIGHT = 400;
	public Thread running;
	private int xCoordinate = WIDTH / 2;
	private int yCoordinate = HEIGHT / 2;
	private final int BALL_R = 20;
	public int xRandom = (-5) + (int) (Math.random() * ((5 - (-5)) + 1));
	public int yRandom = (-5) + (int) (Math.random() * ((5 - (-5)) + 1));
	int xVelocity = xRandom;
	int yVelocity = yRandom;

	public WrapAroundBall() {
		running = new Thread(this);
		running.start();

	}

	@Override
	public void run() {
		while (true) {

			int velocity = 20;

			if (xCoordinate > WIDTH) {
				xCoordinate = 0;
			}
			if (xCoordinate < 0)
				xCoordinate = WIDTH;
			if (yCoordinate > HEIGHT) {
				yCoordinate = 0;
			}
			if (yCoordinate < 0)
				yCoordinate = HEIGHT;

			xCoordinate += xVelocity;
			yCoordinate += yVelocity;

			repaint();
			try {
				Thread.sleep(velocity * 2);
			} catch (InterruptedException ex) {
			}
		}

	}

	@Override
	public void paint(Graphics g) {
		this.setBackground(Color.BLUE);
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(Color.YELLOW);
		g2D.fillOval(xCoordinate, yCoordinate, BALL_R * 2, BALL_R * 2);

	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

}
