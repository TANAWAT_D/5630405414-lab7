package dathtanim.tanawat.lab7;

import java.awt.geom.Rectangle2D;

public class pad extends Rectangle2D.Double {

	private static final long serialVersionUID = 1L;
	final static int PAD_WIDTH = 10;
	final static int PAD_HEIGHT = 80;

	public pad(int x) {
		super(x, x, PAD_WIDTH, PAD_HEIGHT);
	}
}
