package dathtanim.tanawat.lab7;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class PongPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private final int WIDTH;
	private final int HEIGHT;
	private final int PAD_WIDTH;
	private final int PAD_LENGTH;
	private final int PAD_HEIGHT;
	private final int BALL_R;
	private int ballX;
	private int ballY;
	private int player1Score;
	private int player2Score;
	private int leftPad;
	private int rightPad;
	PongPaddle leftPaddle;
	PongPaddle rightPaddle;

	Font font = new Font("Serif", Font.BOLD, 42);

	PongPanel() {
		WIDTH = 600;
		HEIGHT = 400;
		PAD_WIDTH = 10;
		PAD_HEIGHT = 150;
		PAD_LENGTH = 80;
		BALL_R = 20;
		ballX = 280;
		ballY = 180;
		player1Score = 0;
		player2Score = 0;
		leftPaddle = new PongPaddle(0, (HEIGHT / 2) - PAD_LENGTH / 2);
		rightPaddle = new PongPaddle(WIDTH - PAD_WIDTH, (HEIGHT / 2) - (PAD_LENGTH / 2));
	}

	@Override
	public void paintComponent(Graphics g) {
		this.setBackground(Color.BLACK);
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(Color.WHITE);
		g2D.fill(new ball(ballX, ballY, BALL_R));
		g2D.fillRect(leftPaddle.getxCoordinate(), leftPaddle.getyCoordinate(), PAD_WIDTH, PAD_LENGTH);
		g2D.fillRect(rightPaddle.getxCoordinate(), rightPaddle.getyCoordinate(), PAD_WIDTH, PAD_LENGTH);
		g2D.drawLine(getSize().width / 2, WIDTH, getSize().width / 2, 0);
		g2D.drawLine(PAD_WIDTH, WIDTH, PAD_WIDTH, 0);
		g2D.drawLine(getSize().width - PAD_WIDTH, WIDTH, getSize().width - PAD_WIDTH, 0);
		g2D.setFont(font);
		g2D.drawString(String.valueOf(player1Score), WIDTH * 1 / 4, HEIGHT / 4);
		g2D.drawString(String.valueOf(player2Score), WIDTH * 3 / 4, HEIGHT / 4);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

}
