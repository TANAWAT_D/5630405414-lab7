package dathtanim.tanawat.lab7;

import java.awt.Dimension;

import java.awt.Toolkit;

import javax.swing.JFrame;

import javax.swing.SwingUtilities;

public class PongGame extends JFrame {

	private static final long serialVersionUID = 1L;

	protected PongGame(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				PongGame PongPanel = new PongGame("CoE Pong Game");
				PongPanel.createAndShowGUI();
			}
		});

	}

	protected void createAndShowGUI() {
		createComponents();
		setFrameFeatures();

	}

	private void createComponents() {
		add(new PongPanel());

	}

	private void setFrameFeatures() {

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
}
