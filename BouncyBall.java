package dathtanim.tanawat.lab7;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class BouncyBall extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;

	private final static int WIDTH = 600;
	private final static int HEIGHT = 400;
	public Thread running;
	private int xCoordinate = WIDTH / 2;
	private int yCoordinate = HEIGHT / 2;
	private final int BALL_R = 20;
	public int xRandom = (-5) + (int) (Math.random() * ((5 - (-5)) + 1));
	public int yRandom = (-5) + (int) (Math.random() * ((5 - (-5)) + 1));
	int xVelocity = xRandom;
	int yVelocity = yRandom;
	private int bounceTime = 0;
	private boolean increaseSpeed = false;

	public BouncyBall() {

		running = new Thread(this);
		running.start();

	}

	@Override
	public void run() {
		while (true) {
			int velocity = 20;

			if (xCoordinate + xVelocity > WIDTH - BALL_R *2 || 
				xCoordinate + xVelocity < 0) {
				
				xVelocity = -xVelocity;
				bounceTime++;
				
				if (bounceTime != 0 && bounceTime % 5 == 0) {
					increaseSpeed = true;
					xVelocity += xVelocity;
					yVelocity += yVelocity;
				}
			}
			if (yCoordinate + yVelocity > HEIGHT - BALL_R *2|| 
				yCoordinate + yVelocity < 0) {
				
				yVelocity = -yVelocity;
				bounceTime++;
				if (bounceTime != 0 && bounceTime % 5 == 0) {
					increaseSpeed = true;
					xVelocity += xVelocity;
					yVelocity += yVelocity;
				}
			}
			xCoordinate += xVelocity;
			yCoordinate += yVelocity;

			repaint();
			try {
				Thread.sleep(velocity);

			} catch (InterruptedException ex) {

			}
		}
	}

	@Override
	public void paint(Graphics g) {
		this.setBackground(Color.BLACK);
		super.paintComponent(g);
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(Color.WHITE);
		g2D.fillOval(xCoordinate, yCoordinate, BALL_R * 2, BALL_R * 2);

	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);
	}

	public boolean isIncreaseSpeed() {
		return increaseSpeed;
	}

	public void setIncreaseSpeed(boolean increaseSpeed) {
		this.increaseSpeed = increaseSpeed;
	}

}
